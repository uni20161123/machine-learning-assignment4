load GaussianData.mat TrnX TrnY TstX XGrid YGrid;

%%% hyperparameters
    OutputSize = 1;
    LayerSize = [10,10,10,OutputSize];
%%% hyperparameters

plot3(TrnX(:,1)',TrnX(:,2)',TrnY,'.r','MarkerSize',20);

%%% visualize training data (TrnX and TrnY) and test inputs (TstX)
    Cls1Idx = find(TrnY(:,1)>0);
    Cls2Idx = find(TrnY(:,1)==0);

    figure(1);
    subplot(1,3,1);
    YGridSize = length(YGrid);
    XGridSize = length(XGrid);
    ZDat = zeros(YGridSize,XGridSize);
    surf(XGrid,YGrid,ZDat,'EdgeColor','none');
    hold on;
    plot3(TrnX(Cls1Idx,1)',TrnX(Cls1Idx,2)',zeros(length(Cls1Idx),1)*2,'.r','MarkerSize',20);
    plot3(TrnX(Cls2Idx,1)',TrnX(Cls2Idx,2)',ones(length(Cls2Idx),1),'.b','MarkerSize',20);
    view(2);
    axis tight;
    hold off;
%%% visualize training data (TrnX and TrnY) and test inputs (TstX)

%%% MLP training and evaluation
Mdl = MLPTrain(TrnX,TrnY, LayerSize);
TstYPrediction = MLPTest(Mdl,TstX);
% Test code
TrnYPrediction = MLPTest(Mdl, TrnX);
[ysize, ~] = size(TrnYPrediction);
GroundTruth=0;
for i=1:ysize
    if TrnYPrediction(i) == TrnY(i)
        GroundTruth=GroundTruth+1;
    end
end

disp(GroundTruth/ysize * 100);
%     net = feedforwardnet(LayerSize);
%     net = train(net,TrnX',TrnY');
%     view(net);
%     TstYPrediction = net(TstX');
%     TstYPrediction = TstYPrediction';
%%% decision tree training and evaluation

ZDat = zeros(YGridSize,XGridSize);
SIdx = 1;
for j=1:XGridSize
    for k=1:YGridSize
        ZDat(k,j) = TstYPrediction(SIdx,1);
        SIdx = SIdx+1;
    end
end

%%% visualize training data (TrnX and TrnY) and predictions (TstYPrediction) made for test inputs (TstX)
    subplot(1,3,2);
    surf(XGrid,YGrid,ZDat,'EdgeColor','none');
    hold on;
    plot3(TrnX(Cls1Idx,1)',TrnX(Cls1Idx,2)',TrnY(Cls1Idx,1),'.r','MarkerSize',20);
    plot3(TrnX(Cls2Idx,1)',TrnX(Cls2Idx,2)',TrnY(Cls2Idx,1),'.b','MarkerSize',20);
    hold off;

    subplot(1,3,3);
    surf(XGrid,YGrid,ZDat,'EdgeColor','none');
    hold on;
    plot3(TrnX(Cls1Idx,1)',TrnX(Cls1Idx,2)',ones(length(Cls1Idx),1)*2,'.r','MarkerSize',20);
    plot3(TrnX(Cls2Idx,1)',TrnX(Cls2Idx,2)',ones(length(Cls2Idx),1)*2,'.b','MarkerSize',20);
    hold off;
    axis tight;
    view(2);
%%% visualize training data (TrnX and TrnY) and predictions (TstYPrediction) made for test inputs (TstX)

disp('done.');
