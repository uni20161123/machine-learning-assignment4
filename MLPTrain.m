function Mdl = MLPTrain(TrnX,TrnY,LayerSize)
    %%%a variable `Mdl�� that contains the trained MLP model
    updateBias = 1;
    MAX_ITER = 100;
    alpha = 1;
    learning_rate = 0.01;
    [ xrow, ~ ] = size(TrnX);
    [ ~, lcol ] = size(LayerSize);


    Sigmoid = @(a) 1./(1 + exp(-alpha * a));
    Mdl = struct("hlayer_weight", [], "bias", []);
    for i = 1:lcol
        base = 2;
        if i ~= 1
            base = LayerSize(i-1);
        end
        Mdl(i).hlayer_weight = randn(LayerSize(i), base);
        Mdl(i).bias = zeros(LayerSize(i),1);
    end
    %output layer
    Mdl(lcol+1).hlayer_weight = randn(1,1);
    Mdl(lcol+1).bias = zeros(1,1);
    % feed forward
    for iter = 1:MAX_ITER
        for i = 1:xrow
           input = transpose(TrnX(i, :));
           layer = struct("output", []);
           for j = 1:lcol
               [ ~ , hlayercol]= size(Mdl(j).hlayer_weight);
               layer(j).output = zeros(1, hlayercol);
               layer(j).output = Sigmoid(Mdl(j).hlayer_weight * input + Mdl(j).bias);
               input = layer(j).output;
           end
            % output layerfeed forward
           layer(lcol+1).output =  Sigmoid(Mdl(lcol+1).hlayer_weight * input + Mdl(lcol+1).bias);
            % backpropagation with SGD
           costPrime = layer(lcol+1).output - TrnY(i);
           for j = lcol+1:-1:1
               sigPrime = layer(j).output .* ( 1 - layer(j).output );
               CPrime = costPrime .* sigPrime;
               if j == 1
                   if updateBias == 1
                       matrix_one = transpose(TrnX(i, :));
                   end
                   Mdl(j).hlayer_weight = Mdl(j).hlayer_weight - learning_rate * CPrime * TrnX(i, :);
               else
                   if updateBias == 1
                       matrix_one = layer(j-1).output;
                   end

                   costPrime =  transpose(Mdl(j).hlayer_weight) * CPrime;
                   Mdl(j).hlayer_weight = Mdl(j).hlayer_weight - learning_rate * CPrime * transpose(layer(j-1).output);
               end
               
               if updateBias == 1
                   matrix_one = 1;
                   Mdl(j).bias = Mdl(j).bias - learning_rate * CPrime * transpose(matrix_one);
               end

           end
        end
    end
end

function out = ReLU(a)
    alpha = 0;
    [rowa, cola] = size(a);
    out = zeros(rowa, cola);
    for i = 1:rowa
        for j = 1:cola
            if a(i,j) < 0
                out(i,j) = alpha * a(i,j);
            else
                out(i,j) = a(i,j);
            end
        end
    end
end