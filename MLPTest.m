function TstYPrediction = MLPTest(Mdl,TstX)
    %%%Output TstYPrediction which the i-th element is the prediction made by the trained decision tree for the test input��
   alpha = 1;

   [~,mdlcol] = size(Mdl);
   [xrow, ~ ] = size(TstX);
   TstYPrediction = zeros(xrow,1);
   Sigmoid = @(a) 1./(1 + exp( -alpha * a));
   
   for i = 1:xrow
       input=transpose(TstX(i, :));
       layer = struct("output", []);
       for j = 1:mdlcol
           [ ~ , hlayercol]= size(Mdl(j).hlayer_weight);
           layer(j).output = zeros(1, hlayercol);
           layer(j).output = Sigmoid(Mdl(j).hlayer_weight * input + Mdl(j).bias);
           input = layer(j).output;
       end
%        TstYPrediction(i)=layer(j).output;
       if layer(mdlcol).output >= 0.5
           TstYPrediction(i) = 1;
       else
           TstYPrediction(i) = 0;
       end
   end
end


function out = ReLU(a)
    alpha = 0;
    [rowa, cola] = size(a);
    out = zeros(rowa, cola);
    for i = 1:rowa
        for j = 1:cola
            if a(i,j) < 0
                out(i,j) = alpha * a(i,j);
            else
                out(i,j) = a(i,j);
            end
        end
    end
end